Slucaj upotrebe "Iganje jedne partije"

1.Opis:
Igrac zapocinje partiju klikom na dugme Start iz glavnog menija. Aplikacija ucitava informacije o levelu iz .txt fajla i zapocinje igru. Nakon odigranog nivoa (pobeda/poraz) prikazuje se novi ekran sa opcijama(escape/main menu i quit).

2.Akteri:
Igrac - prelazi nivo kretanjem pomocu strelica.

3.Preduslovi:
Aplikacija je pokrenuta i otvoren je glavni meni.

4.Postuslovi: /

5.Osnovni tok:
	1.Igrac bira dugme "Start" iz glavnog menija
	2.Aplikacija konstruise novu partiju i zapocinje je
	3.Aplikacija ucitava level iz fajla i postavlja sve elemente na scenu
	4.U beskonacnoj petlji(bez uslova) ponavljaju se naredni koraci:
		4.1.Ako je igrac pobedio
			4.1.1.Aplikacija prikazuje viktory screen
		4.2.Ako je igrac mrtav
			4.2.1.Aplikacuja prikazuje dead screen
		4.3.Ako igrac umire
			4.3.1.Animira umiranje, postavlja flag da je igrac mrtav.
		4.4.Sve dok ima nestatickih objekata na sceni
			4.4.1.Aplikacija proverava da li je objekat projektil
				4.4.1.Ako jeste
					4.4.1.1. Animiraj ga i pomeri
				4.4.2.Ako nije
					4.4.2.1.Animiraj ga i pomeri
					4.4.2.2.Aplikacija proverava da li je to enemy
						4.4.2.2.1.Ako je ste i ako je mrtav
							4.4.2.2.1.1. Obrisi ga 
		4.5.Ucitaj kontrole igraca
		4.6.Centriraj igraca
		4.7.Centriraj health_bar 
		4.8.Prikaz scene igracu

6.Alternativni tokovi:
A1:Neocekivani izlaz iz aplikacije. Ukoliko prilikom prikaza kontrola korisnik iskljuci aplikaciju, stvari ostaju nepromenjene, level se ne sacuvava i aplikacija zavrsava rad. Slucaj upotrebe se zavrsava.

A2:Ukoliko igrac ubije poslednja 3 neprijatelja kao i princezu(friendly fire enabled), igrac ostaje zarobljen u igri(beskonacna petlja)

7.Podtokovi: /

8.Specijalni zahtevi:/

9.Dodatne informacije:/